<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class Fight {

    use Hewan;

    public $attackPower;
    public $defencePower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ($hewan->attackpower / $this->defencepower) ;
    }

    protected function getInfo()
    {
        echo "Nama : ($this->nama)";
        echo "<br>";
        echo "Jumlah Kaki : ($this->jumlahkaki)";
        echo "<br>";
        echo "Keahlian : ($this->keahlian)";
        echo "<br>";
        echo "Darah : ($this->darah)";
        echo "<br>";
        echo "Attack Power : ($this->attackpower)";
        echo "<br>";
        echo "Defence Power : ($this->defencepower)";
        echo "<br>";
    }

    abstract public function getInfoHewan();

}

class Elang extends Fight{

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackpower = 10;
        $this->defencepower = 5;
    }


    public function getInfoHewan()
    {
     echo "Jenis Hewan : Elang";
     echo "<br>";   

     $this->getInfo();

    }

}

class Harimau extends Fight
{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackpower = 7;
        $this->defencepower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        echo "<br>";
        $this->getInfo();
    }

}

class Spasi {
    public static function tampilkan()
    {
        echo "<br>";
        echo "=========";
        echo "<br>";
    }
}

$harimau = new Harimau("Harimau Sumatra");
$harimau->getInfoHewan();
Spasi::tampilkan();
$elang = new Elang("Elang Jawa");
$elang->getInfoHewan();
Spasi::tampilkan();
$harimau->serang($elang);
Spasi::tampilkan();
$elang->getInfoHewan();
Spasi::tampilkan();

$elang->serang($harimau);
Spasi::tampilkan();
$harimau->getInfoHewan();